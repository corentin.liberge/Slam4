<?php

namespace cinema\consultationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use cinema\consultationBundle\Entity\film;
use cinema\consultationBundle\Entity\studion;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $entityManager= $this->getDoctrine()->getManager();            
        $films=$entityManager->getRepository('cinemaconsultationBundle:film')->findall();
        return $this->render('cinemaconsultationBundle:Default:index1.html.twig', array('films2' => $films));
    }
    
    public function listefilmAction()
    {
        return $this->render('cinemaconsultationBundle:studio:ajoutstudiofilm.html.twig', array());
    }
    
    public function affichefilmAction($id)
    {
        $entityManager= $this->getDoctrine()->getManager();
        $film=$entityManager->find('cinemaconsultationBundle:film',$id);
        echo ($film->getTitre() );
        echo ( $film->getDuree() );
        echo ( $film->getPays() );
        echo ( $film->getResume() );
        echo ( $film->getRecettes() );
        
        
        
        return $this->render('cinemaconsultationBundle:studio:ajoutstudiofilm.html.twig', array());
    }  
        
       public function affichefilm2Action($id)
    {
        $entityManager= $this->getDoctrine()->getManager();
        $film2=$entityManager->find('cinemaconsultationBundle:film',$id);
        return $this->render('cinemaconsultationBundle:Default:unfilm.html.twig', array('film2'=>$film2));
    }   
    
    public function affichelesfilmsAction()
    {
      
              $entityManager= $this->getDoctrine()->getManager();
            
        $films=$entityManager->getRepository('cinemaconsultationBundle:film')->findall();
     
    /*    foreach($films as $film){
         echo ($film->getTitre() ."&nbsp;");
        echo ( $film->getDuree() ."&nbsp;");
        echo ( $film->getPays() ."&nbsp;");
        echo ( $film->getResume()."&nbsp;" );
        echo ( $film->getRecettes()."<br>" );
    }*/
        return $this->render('cinemaconsultationBundle:studio:affichefilmetstudio.html.twig', array('lesfilms' => $films));
    } 
 
    public function affichelesfilms2Action()
    {
           $entityManager= $this->getDoctrine()->getManager();
            
        $films2=$entityManager->getRepository('cinemaconsultationBundle:film')->findall();
        return $this->render('cinemaconsultationBundle:Default:listefilm.html.twig', array('films2' => $films2));
    } 
    
    public function ajoutAction()
    {
        
           $entityManager= $this->getDoctrine()->getManager();
           
           $studio = new \cinema\consultationBundle\Entity\studio();
           $studio->setNom("Lucifer");
           $studio->setPays("Fr");
           $studio->setAdresse("3 rue dark");
           $studio->setCa("72222");           
           $entityManager->persist($studio);
          
           $studio2 = new \cinema\consultationBundle\Entity\studio();
           $studio2->setNom("Angmar");
           $studio2->setPays("Fr");
           $studio2->setAdresse("3 rue klark");
           $studio2->setCa("72500");
           
           $entityManager->persist($studio2);
           $entityManager->flush();
           
          return $this->render('cinemaconsultationBundle:studio:ajoutstudiofilm.html.twig', array());
    }  
    public function ajouterAction()
{
// création de l'objet Film
$leFilm = new film();
// création du FormBuilder avec les champs que l'on souhaite (sauf l'id)
$formBuilder = $this->createFormBuilder($leFilm)
->add('titre', 'text', array('label' => 'Titre'))
->add('duree', 'text', array('label' => 'Durée'))
->add('pays', 'text', array('label' => 'Pays'))
->add('resume', 'text', array('label' => 'Résumé', 'required' => false))
->add('recettes', 'text', array('label' => 'Recettes', 'required' => false));
// génération du formulaire
$form = $formBuilder->getForm();

//récupération de la requète retournée par le submit du formulaire
$request = $this->get('request');
// vérification : la requête est-elle de type POST
if ($request->getMethod() == 'POST') {
/* lien requête <-> formulaire -> les données du formulaire (le nouveau film saisi)
sont désormais dans la variable $leFilm */
$form->bind($request);
// vérification : si les valeurs récupérées sont correctement saisies
if ($form->isValid()) {
// enregistrement du nouveau film dans la BDD
$entityManager = $this->getDoctrine()->getManager();
$entityManager->persist($leFilm);
$entityManager->flush();

// définition d'un message flash
$this->get('session')->getFlashBag()->add('info','Film enregistré');
// puis appel au template permettant d'éditer ce nouveau film
// => on effectue ici une redirection vers une autre page (route)
return $this->redirect($this->generateUrl('cinemaconsultation_affichefilm2',
array('id'=>$leFilm->getId())));
}
}
// appel à la vue avec en paramètre le formulaire créé
return $this->render('cinemaconsultationBundle:Film:ajouter.html.twig', array('form'
=> $form->createView()));
}
public function modifierAction()
    {
    return $this->render('cinemaconsultationBundle:Film:modifier.html.twig', array());
    }
public function supprimerAction()
    {
    return $this->render('cinemaconsultationBundle:Film:supprimer.html.twig', array());
    }
}

