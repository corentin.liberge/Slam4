<?php
/**
* @Entity @Table(name="categorie")
**/
class Categorie
{
/**
* @Id @Column(type="string", length=2)
**/
private $codeCategorie;


/**
* @Column(length=30) 
**/
private $nomCategorie;

// *** puis, constructeurs ainsi que méthodes get et set éventuelles
 public function init($codeCategorie, $nomCategorie)
{
$this->codeCategorie = $codeCategorie;
$this->nomCategorie = $nomCategorie;
}

public function getCodeCategorie()
{
return $this->codeCategorie;
}

public function getNomCategorie()
{
return $this->nomCategorie;
}

public function getLesClients()
{
return $this->lesInstrument;
}

// constructeur par défaut
public function __construct()
{
$this->codeCategorie = "";
$this->nomCategorie = "";
}

}
?>