<?php
/**
* @Entity @Table(name="Instrument")
**/
class Instrument
{
/**
* @Id @Column(type="integer") @GeneratedValue
**/
private $ref;

/**
 *
 * @column(length=30)
 */
private $nom;

/**
 *
 * @column(length=20)
 */
private $marque;
/**
 *
 * @column(length=150)
 */
private $caract;
/**
 *
 * @column(type="integer")
 */
private $prix;
/**
 *
 * @column(length=20)
 */
private $photo;
/**
* @ManyToOne(targetEntity="categorie")
* @JoinColumn (name="codeCategorie", referencedColumnName="codeCategorie")
**/
private $laCategorie;


public function init($ref,$nom,$marque,$caract,$prix,$photo,$laCategorie){
    $this->ref=$ref;
    $this->nom=$nom;
    $this->marque=$marque;
    $this->caract=$caract;
    $this->prix=$prix;
    $this->photo=$photo;
    $this->laCategorie=$laCategorie;
}
public function getRef(){
    return $this->ref;
}
public function getNom(){
    return $this->nom;
}
public function getMarque(){
    $this->marque;
}
public function getCaract(){
    $this->caract;
}
public function getPrix(){
    $this->prix;
}
public function getPhoto(){
    $this->photo;
}


public function __construct()
{
    $this->ref="";
    $this->nom="";
    $this->marque="";
    $this->caract="";
    $this->prix=0;
    $this->photo="";
    $this->laCategorie;
}

}
?>