<?php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
require_once "vendor/autoload.php";
/* Il faut activer une des 3 lignes suivantes en fonction du langage (PHP, XML ou YAML) que l'on va
utiliser pour coder les classes permettant d'accéder à la base de données.
Dans ces 3 lignes, le paramètre des méthodes createAnnotationMetadataConfiguration,
createXMLMetadataConfiguration, et createYAMLMetadataConfiguration spécifie le répertoire dans lequel
sont codées les classes pour l'accès à la base de données.
Le chemin indiqué dans ces 3 lignes correspond à l'arborescence qui a été créée précédemment.*/
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/src"), true);
//$config = Setup::createXMLMetadataConfiguration(array(__DIR__."/config/xml"), true);
//$config = Setup::createYAMLMetadataConfiguration(array(__DIR__."/config/yaml"),true);
//configuration des paramètres d'accès à la BDD
$connexion = array(
'driver' => 'pdo_mysql',
'host' => '172.16.99.3',
'port' => '3306',
'dbname' => 'c.liberge',
'user' => 'c.liberge',
'password' => 'passe',
);
// obtention du gestionnaire d'entités
$entityManager = EntityManager::create($connexion, $config);
?>
